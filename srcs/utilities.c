/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utilities.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/04 15:45:59 by jabadie           #+#    #+#             */
/*   Updated: 2015/05/05 17:11:16 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"

int			treehassymbol(char symbol, t_array *tree)
{
	unsigned int i;

	if (tree->symbol == symbol)
		return (TRUE);
	if (ft_isupper(tree->symbol))
		return (FALSE);
	i = 0;
	while (i < tree->size)
	{
		if (treehassymbol(symbol, array_get(tree, i)))
			return (TRUE);
		i++;
	}
	return (FALSE);
}

int			while_space(char *str, int i)
{
	while (str[i] == ' ' || str[i] == '\t' || str[i] == '\r' || str[i] == '\n')
		i++;
	return (i);
}

void		replace_hashtag(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (str[i] == '#')
		{
			str[i] = 0;
			return ;
		}
		i++;
	}
}

int			expertnot(int value)
{
	if (value == TRUE)
		return (FALSE);
	else if (value == FALSE)
		return (TRUE);
	else
		return (value);
}
