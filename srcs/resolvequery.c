/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resolvequery.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmeier <nmeier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/05 17:05:44 by nmeier            #+#    #+#             */
/*   Updated: 2015/05/07 14:48:36 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"
#include "cluedo.h"

static int		querryused(char query, t_env env)
{
	int i;

	i = 0;
	while (i < *env.usedcount)
	{
		if (env.usedquerys[i] == query)
			return (TRUE);
		i++;
	}
	return (FALSE);
}

static int		checkfacts(char query, t_env env, int level)
{
	int ret;

	if (env.query[query - 'A'] == TRUE)
		ret = UNKNOWN;
	else
		ret = env.alph[query - 'A'];
	if (env.opts & VERBOSE)
	{
		print_tabs(level);
		if (env.opts & COLOR)
			ft_putstr(C_BROWN);
		ft_putchar(query);
		if (ret == TRUE)
			ft_putendl(" is a fact");
		else if (ret == FALSE)
			ft_putendl(" is not a fact");
		else if (ret == UNKNOWN)
			ft_putendl(" is unknown");
		if (env.opts & COLOR)
			ft_putstr(C_NONE);
	}
	return (ret);
}

static void		printnovalidexpression(t_env env, int level, char query)
{
	if (env.opts & VERBOSE)
	{
		print_tabs(level);
		if (env.opts & COLOR)
			ft_putstr(C_MAGENTA);
		ft_putstr("No valid expression found for ");
		ft_putchar(query);
		if (env.opts & COLOR)
			ft_putstr(C_NONE);
		ft_putchar('\n');
		if (env.opts & COLOR)
		{
			ft_putstr(C_UNDERLINE);
			ft_putstr(C_RED);
		}
		print_tabs(level);
		ft_putstr("So ");
		ft_putchar(query);
		ft_putendl(" is false");
		if (env.opts & COLOR)
			ft_putstr(C_NONE);
	}
}

static void		printunknown(t_env env, int level, char query)
{
	if (env.opts & VERBOSE)
	{
		print_tabs(level);
		ft_putchar(query);
		ft_putendl(" is unknown");
	}
}

int				resolvequery(char query, t_env env, int level)
{
	int ret;

	if (querryused(query, env) == TRUE)
	{
		printunknown(env, level, query);
		return (UNKNOWN);
	}
	env.usedquerys[(*env.usedcount)++] = query;
	ret = UNKNOWN;
	if ((ret = checkfacts(query, env, level)) == TRUE || ret == UNKNOWN)
	{
		*env.usedcount -= 1;
		return (ret);
	}
	if ((ret = checkexprs(query, env, level)) == NOEXPR)
	{
		printnovalidexpression(env, level, query);
		*env.usedcount -= 1;
		return (FALSE);
	}
	*env.usedcount -= 1;
	return (ret);
}
