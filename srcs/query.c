/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   query.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/05 17:25:29 by jabadie           #+#    #+#             */
/*   Updated: 2015/05/07 14:47:49 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"

static void	print_before(t_env env, char qs)
{
	if (env.opts & VERBOSE)
	{
		if (env.opts & COLOR)
		{
			ft_putstr(C_BOLD);
			ft_putstr(C_BROWN);
			ft_putstr(C_UNDERLINE);
		}
		ft_putstr("\nQUERY ");
		ft_putchar(qs);
		ft_putendl(":");
		if (env.opts & COLOR)
			ft_putstr(C_NONE);
	}
}

static void	print_after(t_env env, char qs, int ret)
{
	if (ret == NOEXPR)
		ret = FALSE;
	if (env.opts & COLOR)
	{
		if (ret == TRUE)
			ft_putstr(C_GREEN);
		else
			ft_putstr(C_RED);
		ft_putstr(C_BOLD);
		ft_putstr(C_UNDERLINE);
	}
	ft_putchar(qs);
	ft_putstr(" is ");
	ft_putendl(strvalue(ret));
	if (env.opts & COLOR)
		ft_putstr(C_NONE);
}

void		resolve(t_env env)
{
	char	*qs;
	int		ret;
	int		i;

	qs = env.query;
	i = 0;
	while (i < ALPH_SZ)
	{
		if (qs[i] == TRUE)
		{
			print_before(env, 'A' + i);
			ft_bzero(env.usedquerys, sizeof(char) * ALPH_SZ);
			if (env.alph[i] == TRUE)
				ret = TRUE;
			else
				ret = checkexprs('A' + i, env, 0);
			print_after(env, 'A' + i, ret);
		}
		i++;
	}
}
