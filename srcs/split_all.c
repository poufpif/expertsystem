/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_all.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/29 12:20:28 by jabadie           #+#    #+#             */
/*   Updated: 2015/05/05 18:13:23 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"
#include "array.h"

t_array	*split_and(char *str, int found)
{
	char			*tmp;
	t_array			*result;
	unsigned int	i;

	tmp = str;
	str = ft_strtrim(str);
	free(tmp);
	if (str[0] == 0)
		ft_error("Missing something in and", NULL);
	result = array_create(5);
	result = specialsplit("+", str);
	if (result->size == 1)
		return (handle_not(str, found));
	i = 0;
	result->symbol = '+';
	while (i < result->size)
	{
		array_set(result, i, handle_not(array_get(result, i), found));
		i++;
	}
	return (result);
}

t_array	*split_or(char *str, int found)
{
	char			*tmp;
	t_array			*result;
	unsigned int	i;

	tmp = str;
	str = ft_strtrim(str);
	free(tmp);
	if (str[0] == 0)
		ft_error("Missing something in or", NULL);
	result = array_create(5);
	result = specialsplit("|", str);
	if (result->size == 1)
		return (split_and(str, found));
	i = 0;
	result->symbol = '|';
	while (i < result->size)
	{
		array_set(result, i, split_and(array_get(result, i), found));
		i++;
	}
	return (result);
}

t_array	*split_xor(char *str, int found)
{
	char			*tmp;
	t_array			*result;
	unsigned int	i;

	tmp = str;
	str = ft_strtrim(str);
	free(tmp);
	if (str[0] == 0)
		ft_error("Missing something in xor", NULL);
	result = array_create(5);
	result = specialsplit("^", str);
	if (result->size == 1)
		return (split_or(str, found));
	i = 0;
	result->symbol = '^';
	while (i < result->size)
	{
		array_set(result, i, split_or(array_get(result, i), found));
		i++;
	}
	return (result);
}

t_array	*split_implies(char *str, int found)
{
	char			*tmp;
	t_array			*result;
	unsigned int	i;

	tmp = str;
	str = ft_strtrim(str);
	free(tmp);
	if (str[0] == 0)
		ft_error("Missing something in implies", NULL);
	result = array_create(5);
	result = specialsplit("=>", str);
	if (result->size == 1)
		return (split_xor(str, found));
	if (found == TRUE)
		ft_error("Nested implies", NULL);
	found = TRUE;
	if (result->size > 2)
		ft_error("Multiple implies", NULL);
	i = -1;
	result->symbol = '>';
	while (++i < result->size)
		array_set(result, i, split_xor(array_get(result, i), found));
	return (result);
}

t_array	*split_ifoif(char *str, int found)
{
	char			*tmp;
	t_array			*result;
	unsigned int	i;

	tmp = str;
	str = ft_strtrim(str);
	free(tmp);
	if (str[0] == 0)
		ft_error("Missing something in ifoif", NULL);
	result = array_create(5);
	result = specialsplit("<=>", str);
	if (result->size == 1)
		return (split_implies(str, found));
	if (found == TRUE)
		ft_error("Nested ifoif", NULL);
	found = TRUE;
	if (result->size > 2)
		ft_error("Multiple ifoif", NULL);
	i = -1;
	result->symbol = '=';
	while (++i < result->size)
		array_set(result, i, split_implies(array_get(result, i), found));
	return (result);
}
