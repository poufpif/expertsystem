/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   specialsplit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmeier <nmeier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/05 17:35:15 by nmeier            #+#    #+#             */
/*   Updated: 2015/05/05 17:35:17 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "array.h"
#include "dynstr.h"
#include "libft.h"

static int	checkdelim(int *i, int delimlen, const char *string, char *delim)
{
	char		*tmpstr;

	if (delimlen > 1)
	{
		tmpstr = ft_strsub(string, *i, delimlen);
		if (ft_strcmp(tmpstr, delim) != 0)
		{
			*i += delimlen - 1;
			free(tmpstr);
			return (1);
		}
		free(tmpstr);
	}
	return (0);
}

static int	checklevel(int *i, int delimlen, t_dynstr *dynstr, t_array *result)
{
	array_append(result, ft_strdup(dynstr->chars));
	dynstr_clear(dynstr);
	if (delimlen > 1)
		*i += delimlen - 1;
	return (1);
}

static void	loop(const char *str, char *delim, t_dynstr *dstr, t_array *res)
{
	int			i;
	int			delimlen;
	int			level;

	level = 0;
	delimlen = ft_strlen(delim);
	i = -1;
	while (++i < ft_strlen(str))
	{
		if (str[i] == '(')
			level++;
		else if (str[i] == ')')
			level--;
		else if (str[i] == delim[0])
		{
			if (checkdelim(&i, delimlen, str, delim))
				continue ;
			if (level == 0 && checklevel(&i, delimlen, dstr, res))
				continue ;
		}
		dynstr_appendchar(dstr, str[i]);
	}
	if (level != 0)
		ft_error("Error: wrong brackets", NULL);
	array_append(res, ft_strdup(dstr->chars));
}

t_array		*specialsplit(char *delim, const char *string)
{
	t_array		*result;
	t_dynstr	*dynstr;

	dynstr = dynstr_alloc("", 10);
	result = array_create(5);
	loop(string, delim, dynstr, result);
	dynstr_destroy(dynstr);
	return (result);
}
