/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmeier <nmeier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/05 16:51:59 by nmeier            #+#    #+#             */
/*   Updated: 2015/05/05 16:52:01 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "array.h"
#include <assert.h>
#include <stdlib.h>

t_array					*array_create(unsigned int cap)
{
	t_array *array;

	array = (t_array*)malloc(sizeof(t_array));
	if (array)
	{
		array->size = 0;
		array->cap = cap;
		array->data = malloc(sizeof(void*) * cap);
		if (!array->data)
		{
			free(array);
			array = NULL;
		}
	}
	return (array);
}

void					array_destroy(t_array *array)
{
	if (array != NULL)
	{
		free(array->data);
		free(array);
	}
}

void					array_append(t_array *array, void *value)
{
	assert(array != NULL);
	if (array->size >= array->cap)
	{
		array->cap *= 2;
		array->data = realloc(array->data, sizeof(void*) * array->cap);
		assert(array->data);
	}
	array->data[array->size] = value;
	array->size++;
}

void					*array_get(t_array *array, unsigned int index)
{
	assert(index < array->size);
	return (array->data[index]);
}

void					array_set(t_array *arr, unsigned int index, void *val)
{
	assert(index < arr->size);
	arr->data[index] = val;
}
