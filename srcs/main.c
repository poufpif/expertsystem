/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expert.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/04 14:59:09 by jabadie           #+#    #+#             */
/*   Updated: 2015/05/04 19:01:08 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"

static void		printexprs(t_env env)
{
	t_expr	*elist;

	elist = env.list;
	while (elist != NULL)
	{
		print_r(elist->left_side, 0);
		ft_putstr("\t=> ");
		print_r(elist->right_side, 0);
		ft_putchar('\n');
		elist = elist->next;
	}
}

static void		printinput(t_env env)
{
	int		i;

	if (env.opts & COLOR)
	{
		ft_putstr(C_BOLD);
		ft_putstr(C_BROWN);
	}
	ft_putendl("\nINPUT:");
	printexprs(env);
	ft_putstr("=");
	i = -1;
	while (++i < ALPH_SZ)
		if (env.alph[i])
			ft_putchar('A' + i);
	ft_putstr("\n?");
	i = -1;
	while (++i < ALPH_SZ)
		if (env.query[i])
			ft_putchar('A' + i);
	if (env.opts & COLOR)
		ft_putstr(C_NONE);
	ft_putchar('\n');
}

int				see_flag(char c, t_env *env)
{
	if (c == 'v')
		env->opts |= VERBOSE;
	else if (c == 'e')
		env->opts |= ECHOINPUT;
	else if (c == 'G')
		env->opts |= COLOR;
	else if (c == 'a')
		env->opts |= ASCIIART;
	else if (c == 'o')
		env->opts |= ORCONCL;
	else if (c == 'i')
		env->opts |= IFOIF;
	else if (c == 'x')
		env->opts |= XORCONCL;
	else if (c == 'C')
		env->opts |= (CLUEDO | ECHOINPUT | VERBOSE | COLOR | IFOIF | ORCONCL);
	else if (c == '\0')
		return (0);
	else
		ft_error("Unknown option: ", &c);
	return (1);
}

int				see_args(char **av, int ac, t_env *env)
{
	int	i;
	int	i2;

	i = 1;
	while (i < ac && av[i][0] == '-')
	{
		i2 = 1;
		if (av[i][1] == '\0')
			break ;
		while (see_flag(av[i][i2], env) == 1)
			i2++;
		if (av[i][i2] != '\0')
			break ;
		i++;
	}
	return (i);
}

int				main(int ac, char **av)
{
	int		fd;
	int		i;
	t_env	env;

	if (ac < 2)
		ft_error("You must put an input file", NULL);
	env = init_env();
	i = see_args(av, ac, &env);
	if ((fd = open(av[i], O_RDONLY)) == -1)
		ft_error("No file with this name: ", av[i]);
	parser_chief(fd, &env);
	if (env.list == NULL)
		return (0);
	if (env.opts & ECHOINPUT)
		printinput(env);
	validate_facts(env);
	resolve(env);
	return (0);
}
