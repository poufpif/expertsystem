/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmeier <nmeier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 15:02:54 by nmeier            #+#    #+#             */
/*   Updated: 2015/05/07 15:03:06 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARRAY_H
# define ARRAY_H

typedef struct	s_array
{
	char			symbol;
	unsigned int	size;
	unsigned int	cap;
	void			**data;
}				t_array;

t_array			*array_create(unsigned int cap);
void			array_destroy(t_array *array);
void			array_append(t_array *array, void *value);
void			*array_get(t_array *array, unsigned int index);
void			array_set(t_array *array, unsigned int index, void *value);

#endif
