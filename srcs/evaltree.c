/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   evaltree.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmeier <nmeier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/05 17:03:10 by nmeier            #+#    #+#             */
/*   Updated: 2015/05/05 17:40:59 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"

static int	eval_and(t_array *tree, t_env env, int level)
{
	unsigned int	i;
	int				ret;

	i = 0;
	while (i < tree->size)
	{
		ret = evaltree(array_get(tree, i), env, level);
		if (ret != TRUE)
			return (ret);
		i++;
	}
	return (TRUE);
}

static int	eval_or(t_array *tree, t_env env, int level)
{
	unsigned int	i;
	int				ret;
	int				eret;

	eret = FALSE;
	i = 0;
	while (i < tree->size)
	{
		ret = evaltree(array_get(tree, i), env, level);
		if (ret == TRUE)
			return (TRUE);
		if (ret == UNKNOWN)
			eret = UNKNOWN;
		i++;
	}
	return (eret);
}

static int	eval_xor(t_array *tree, t_env env, int level)
{
	unsigned int	i;
	int				ret;
	int				prevret;

	i = 1;
	prevret = evaltree(array_get(tree, 0), env, level);
	if (prevret == UNKNOWN)
		return (UNKNOWN);
	while (i < tree->size)
	{
		ret = evaltree(array_get(tree, i), env, level);
		if (ret == UNKNOWN)
			return (UNKNOWN);
		if (prevret == ret)
			return (FALSE);
		prevret = ret;
		i++;
	}
	return (TRUE);
}

static int	eval_not(t_array *tree, t_env env, int level)
{
	return (expertnot(evaltree(array_get(tree, 0), env, level)));
}

int			evaltree(t_array *tree, t_env env, int level)
{
	if (tree->symbol == '+')
		return (eval_and(tree, env, level));
	if (tree->symbol == '|')
		return (eval_or(tree, env, level));
	if (tree->symbol == '^')
		return (eval_xor(tree, env, level));
	if (tree->symbol == '!')
		return (eval_not(tree, env, level));
	else
		return (resolvequery(tree->symbol, env, level + 1));
}
