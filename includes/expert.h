/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expert.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmeier <nmeier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 15:05:50 by nmeier            #+#    #+#             */
/*   Updated: 2015/05/07 15:08:14 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EXPERT_H
# define EXPERT_H

# include "../libft/libft.h"
# include <fcntl.h>
# include "array.h"

# define ALPH_SZ 26
# define C_NONE         "\033[0m"
# define C_BOLD         "\033[1m"
# define C_UNDERLINE    "\033[4m"
# define C_BLACK        "\033[30m"
# define C_RED          "\033[31m"
# define C_GREEN        "\033[32m"
# define C_BROWN        "\033[33m"
# define C_BLUE         "\033[34m"
# define C_MAGENTA      "\033[35m"
# define C_CYAN         "\033[36m"
# define C_GRAY         "\033[37m"

typedef enum	e_state
{
	FALSE = 0,
	TRUE = 1,
	UNKNOWN = 2,
	NOEXPR = 3,
	ARREND = 4,
	ERROR = -1
}				t_state;

typedef enum	e_symbols
{
	NOT_CHAR = '!',
	AND_CHAR = '+',
	OR_CHAR = '|',
	XOR_CHAR = '^',
	IMPLI_CHAR = '>',
	IFOIF_CHAR = '='
}				t_symbols;

typedef enum	e_options
{
	VERBOSE = 0x01,
	ECHOINPUT = 0x02,
	COLOR = 0x04,
	CLUEDO = 0x08,
	ASCIIART = 0x10,
	ORCONCL = 0x20,
	IFOIF = 0x40,
	XORCONCL = 0x80
}				t_options;

typedef struct	s_expr
{
	t_array			*left_side;
	t_array			*right_side;
	int				id;
	struct s_expr	*next;
}				t_expr;

typedef struct	s_env
{
	t_expr	*list;
	char	*query;
	char	*alph;
	char	*cache;
	int		opts;
	char	*usedquerys;
	int		*usedexprs;
	int		*usedcount;
	int		*usedexprcount;
	int		exprcount;
	char	validation;
}				t_env;

/*
**INIT FUNCTION
*/
t_env			init_env(void);
t_expr			*create_expr(void);

/*
**UTILITIES
*/
int				while_space(char *str, int i);
void			replace_hashtag(char *str);
int				expertnot(int value);
void			print_r(t_array *array, int level);
void			print_tabs(int nbr);
char			*strvalue(int value);
void			print_verbose_xor_or(int level, t_array *query, t_array *tree, \
		int ret);
void			printeval(char query, t_expr *expr, t_env env, int level);
void			printrighttreeresult(char query, t_env env, int level, int ret);
void			printtrueexpression(t_expr *expr, t_env env, int level);
void			printreslist(char *rets, int count, int level, t_env env);

/*
**PARSER
*/
void			parser_chief(int fd, t_env *env);
void			mode_queryinit(char *str, char *tab);
void			mode_factinit(char *str, char *tab);
t_array			*split_ifoif(char *str, int found);
t_array			*handle_not(char *str, int found);
t_array			*specialsplit(char *delim, const char *string);
t_expr			*validate_expr(t_array *expr, t_env env);
void			validate_facts(t_env env);

/*
**RESOLVER
*/
void			resolve(t_env env);
int				resolvequery(char query, t_env env, int level);
int				evaltree(t_array *tree, t_env env, int level);
int				evalrighttree(char query, t_array *tree, t_env env, int level);
int				treehassymbol(char symbol, t_array *tree);
int				checkexprs(char query, t_env env, int level);

#endif
