/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cluedo.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmeier <nmeier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 14:32:45 by nmeier            #+#    #+#             */
/*   Updated: 2015/05/07 14:32:46 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cluedo.h"

char *fact_string(char fact)
{
	if (fact == 'P')
		return ("Vegeta is the murderer");
	if (fact == 'Q')
		return ("Other is the murderer");
	if (fact == 'R')
		return ("Jp is the murderer");
	else
		return ("");
}
