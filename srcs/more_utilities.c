/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   more_utilities.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/05 17:09:00 by jabadie           #+#    #+#             */
/*   Updated: 2015/05/05 17:11:15 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"

static int	getprio(int symbol)
{
	if (symbol == '=')
		return (7);
	if (symbol == '>')
		return (6);
	if (symbol == '^')
		return (5);
	if (symbol == '|')
		return (4);
	if (symbol == '+')
		return (3);
	if (symbol == '!')
		return (2);
	return (1);
}

static int	moreprecedence(char a, char b)
{
	return (getprio(a) > getprio(b));
}

void		print_r(t_array *array, int level)
{
	unsigned int	i;
	t_array			*child;

	if (ft_isupper(array->symbol))
	{
		ft_putchar(array->symbol);
		return ;
	}
	i = 0;
	while (i < array->size)
	{
		child = array_get(array, i);
		if (i > 0 || array->size == 1)
			ft_putchar(array->symbol);
		if (moreprecedence(child->symbol, array->symbol))
			ft_putchar('(');
		print_r(array_get(array, i), level + 1);
		if (moreprecedence(child->symbol, array->symbol))
			ft_putchar(')');
		i++;
	}
}

void		print_tabs(int nbr)
{
	while (nbr-- > 0)
		ft_putchar('\t');
}

char		*strvalue(int value)
{
	if (value == TRUE)
		return ("true");
	if (value == FALSE)
		return ("false");
	if (value == UNKNOWN)
		return ("unknown");
	if (value == NOEXPR)
		return ("no expr");
	return ("error");
}
