/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/04 15:36:16 by jabadie           #+#    #+#             */
/*   Updated: 2015/05/07 14:36:51 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"

int			getnextid(void)
{
	static int nextid = 0;

	return (nextid++);
}

t_expr		*create_expr(void)
{
	t_expr	*new;

	if (!(new = (t_expr*)malloc(sizeof(t_expr))))
		ft_error("Malloc Error", NULL);
	new->left_side = NULL;
	new->right_side = NULL;
	new->next = NULL;
	new->id = getnextid();
	return (new);
}

static void	initcache(t_env *env)
{
	int i;

	if (!(env->cache = ft_strnew(ALPH_SZ - 1)))
		ft_error("Strnew Error", NULL);
	i = 0;
	while (i < ALPH_SZ)
	{
		env->cache[i] = UNKNOWN;
		i++;
	}
}

t_env		init_env(void)
{
	t_env	env;

	ft_bzero(&env, sizeof(env));
	env.list = create_expr();
	if (!(env.query = ft_strnew(ALPH_SZ - 1)))
		ft_error("Strnew Error", NULL);
	if (!(env.alph = ft_strnew(ALPH_SZ - 1)))
		ft_error("Strnew Error", NULL);
	if (!(env.usedquerys = ft_strnew(ALPH_SZ - 1)))
		ft_error("Strnew Error", NULL);
	if (!(env.usedcount = malloc(sizeof(int))))
		ft_error("Malloc Error", NULL);
	if (!(env.usedexprcount = malloc(sizeof(int))))
		ft_error("Malloc Error", NULL);
	if (!(env.usedexprs = malloc(sizeof(int) * 100)))
		ft_error("Malloc Error", NULL);
	*env.usedcount = 0;
	*env.usedexprcount = 0;
	initcache(&env);
	return (env);
}
