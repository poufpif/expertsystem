/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validate_facts.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/05 17:14:09 by jabadie           #+#    #+#             */
/*   Updated: 2015/05/05 17:14:10 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"

static void	print_before(t_env env)
{
	if (env.opts & VERBOSE)
	{
		ft_putchar('\n');
		if (env.opts & COLOR)
		{
			ft_putstr(C_BROWN);
			ft_putstr(C_BOLD);
			ft_putstr(C_UNDERLINE);
		}
		ft_putstr("VALIDATING FACTS:");
		if (env.opts & COLOR)
			ft_putstr(C_NONE);
		ft_putchar('\n');
	}
}

static void	print_after(t_env env)
{
	if (env.opts & VERBOSE)
	{
		if (env.opts & COLOR)
		{
			ft_putstr(C_GREEN);
			ft_putstr(C_BOLD);
			ft_putstr(C_UNDERLINE);
		}
		ft_putstr("ALL FACTS ARE VALID");
		if (env.opts & COLOR)
			ft_putstr(C_NONE);
		ft_putchar('\n');
	}
}

void		validate_facts(t_env env)
{
	int i;
	int ret;

	env.validation = TRUE;
	print_before(env);
	i = 0;
	while (i < ALPH_SZ)
	{
		if (env.alph[i] == TRUE
				&& (ret = checkexprs('A' + i, env, 0)) != NOEXPR)
		{
			if (ret == FALSE)
				ft_error("Contradiction between facts and expressions", NULL);
		}
		i += 1;
	}
	print_after(env);
	env.validation = FALSE;
}
