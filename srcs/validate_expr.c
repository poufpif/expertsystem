/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validate_expr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/05 17:15:02 by jabadie           #+#    #+#             */
/*   Updated: 2015/05/07 14:50:08 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"

static int	checkroot(t_array *expr, t_env env)
{
	if (expr->symbol != IMPLI_CHAR && (!(env.opts & IFOIF)
				|| expr->symbol != IFOIF_CHAR))
	{
		ft_putstr("!! Wrong root symbol '");
		ft_putchar(expr->symbol);
		ft_putstr("' in expression \"");
		print_r(expr, 0);
		ft_putendl("\", ignoring it");
		return (FALSE);
	}
	return (TRUE);
}

static int	checkor(t_array *expr, t_env env)
{
	if (!(env.opts & ORCONCL) && treehassymbol(OR_CHAR, array_get(expr, 1)))
	{
		ft_putstr("!! Or is not supported in conclusions, ignoring \"");
		print_r(expr, 0);
		ft_putendl("\"");
		return (FALSE);
	}
	return (TRUE);
}

static int	checkxor(t_array *expr, t_env env)
{
	if (!(env.opts & XORCONCL) && treehassymbol(XOR_CHAR, array_get(expr, 1)))
	{
		ft_putstr("!! Xor is not supported in conclusions, ignoring \"");
		print_r(expr, 0);
		ft_putendl("\"");
		return (FALSE);
	}
	return (TRUE);
}

static int	checkcontr(t_array *expr)
{
	char	lsym;
	t_array	*left;
	t_array	*right;

	left = array_get(expr, 0);
	right = array_get(expr, 1);
	lsym = left->symbol;
	if (ft_isupper(lsym))
	{
		if (right->symbol == NOT_CHAR)
			if (((t_array*)array_get(right, 0))->symbol == lsym)
			{
				ft_putstr("!! Contradiction, ignoring \"");
				print_r(expr, 0);
				ft_putendl("\"");
				return (FALSE);
				return (0);
			}
	}
	return (1);
}

t_expr		*validate_expr(t_array *expr, t_env env)
{
	t_expr	*result;
	void	*tmp;

	if (!checkroot(expr, env) || !checkor(expr, env)
			|| !checkxor(expr, env) || !checkcontr(expr))
		return (NULL);
	result = create_expr();
	result->left_side = array_get(expr, 0);
	result->right_side = array_get(expr, 1);
	if (expr->symbol == IFOIF_CHAR)
	{
		tmp = array_get(expr, 0);
		array_set(expr, 0, array_get(expr, 1));
		array_set(expr, 1, tmp);
		if (!checkor(expr, env) || !checkxor(expr, env) || !checkcontr(expr))
			return (NULL);
		result->next = create_expr();
		result->id -= 1;
		result->next->left_side = array_get(expr, 0);
		result->next->right_side = array_get(expr, 1);
	}
	return (result);
}
