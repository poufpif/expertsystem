/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_not_bracket.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/05 17:35:30 by jabadie           #+#    #+#             */
/*   Updated: 2015/05/05 17:38:13 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"
#include "array.h"

t_array	*brackets(char *str, int found)
{
	char	*tmp;
	t_array	*result;

	tmp = str;
	str = ft_strtrim(str);
	free(tmp);
	if (str[0] == 0)
		ft_error("Missing something in brackets", NULL);
	result = array_create(5);
	if (str[0] == '(')
		result = split_ifoif(ft_strndup(str + 1, ft_strlen(str) - 2), found);
	else
	{
		if ((!ft_isupper(str[0])) || str[1] != '\0')
			ft_error("Wrong symbol: ", str);
		result->symbol = str[0];
	}
	return (result);
}

t_array	*handle_not(char *str, int found)
{
	char			*tmp;
	t_array			*result;
	unsigned int	i;

	tmp = str;
	str = ft_strtrim(str);
	free(tmp);
	if (str[0] == 0)
		ft_error("Missing something in not", NULL);
	result = array_create(5);
	result = specialsplit("!", str);
	if (result->size == 1)
		return (brackets(str, found));
	else if (result->size > 2)
		ft_error("Multiple not", NULL);
	i = 0;
	result->symbol = '!';
	while (i < result->size)
	{
		if (i > 0)
			array_set(result, i - 1, brackets(array_get(result, i), found));
		i++;
	}
	result->size -= 1;
	return (result);
}
