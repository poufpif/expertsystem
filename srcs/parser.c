/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/04 15:16:49 by jabadie           #+#    #+#             */
/*   Updated: 2015/05/07 14:47:24 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"
#include <stdbool.h>

void			alphinit(char *str, char *tab)
{
	int			i;

	str += while_space(str, 0);
	i = 0;
	while (str[i])
	{
		if (ft_isupper(str[i]))
			tab[str[i] - 'A'] = TRUE;
		else
			ft_error("Bad input: ", &str[i]);
		i = while_space(str, i + 1);
	}
}

static t_expr	*parseline(char *str, t_env *env)
{
	int		i;
	char	*ptr;
	t_array	*array;
	t_expr	*expr;

	i = while_space(str, 0);
	if (ft_strlen(str + i) == 0)
		return (NULL);
	if (str[i] == '?')
		alphinit(str + i + 1, env->query);
	else if (str[i] == '=')
		alphinit(str + i + 1, env->alph);
	else if ((ptr = ft_strchr(str, '=')) != NULL)
	{
		array = split_ifoif(ft_strdup(str), FALSE);
		expr = validate_expr(array, *env);
		return (expr);
	}
	return (NULL);
}

int				parsr_help(t_expr *currexpr, int first, t_env *env,
		t_expr **list)
{
	env->exprcount += 1;
	if (!first)
	{
		(*list)->next = currexpr;
		*list = (*list)->next;
	}
	else
	{
		*list = currexpr;
		first = FALSE;
		env->list = *list;
	}
	while ((*list)->next != NULL)
		*list = (*list)->next;
	return (first);
}

static void		addquerystoused(t_env env)
{
	int i;

	i = 0;
	while (i < ALPH_SZ)
	{
		if (env.query[i] == TRUE)
		{
			env.usedquerys[(*env.usedcount)++] = 'A' + i;
		}
		i++;
	}
}

void			parser_chief(int fd, t_env *env)
{
	char	*line;
	t_expr	*currexpr;
	t_expr	*list;
	int		first;

	list = NULL;
	env->list = NULL;
	first = TRUE;
	while (get_next_line(fd, &line) > 0)
	{
		replace_hashtag(line);
		currexpr = parseline(line, env);
		if (currexpr != NULL)
			first = parsr_help(currexpr, first, env, &list);
		free(line);
	}
	addquerystoused(*env);
}
