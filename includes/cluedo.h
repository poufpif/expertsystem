/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cluedo.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmeier <nmeier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 14:56:25 by nmeier            #+#    #+#             */
/*   Updated: 2015/05/07 15:00:38 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLUEDO_H
# define CLUEDO_H

/*
** A => vegeta was seen with the chandelier
** B => vegeta was seen with a knife
** C => vegeta has powder on his hands
** DEF => other weapons
** GHI => jp weapons
** -
** J => vegeta was seen in the kitchen
** K => vegeta was seen in the bathroom
** LM => other rooms
** NO => jp rooms
** -
** P => vegeta is the murderer
** Q => other is the murderer
** R => jp is the murderer
** -
** S => victim was killed with a chandelier
** T => victim was killed with a knife
** U => victim was killed with a gun
** -
** V => victim was killed in the kitchen
** W => victim was killed in the lobby
** -
** X => whoo killed victim?
*/

char	*fact_string(char fact);

#endif
