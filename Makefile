# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jabadie <jabadie@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/07 15:26:23 by jabadie           #+#    #+#              #
#    Updated: 2015/04/05 13:58:55 by nmeier           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = expert
SRC_PATH = ./srcs/
OBJ_PATH = ./objs/

SRCS = $(shell ls srcs)
CFLAGS = -g -Wall -Wextra -Werror
LDFLAGS = -lft

INCPATH = ./includes/
INCLIB = ./libft/
LIBPATH = ./libft/

OBJ = $(SRCS:.c=.o)
SRC = $(addprefix $(SRC_PATH),$(SRCS))
OBJS = $(addprefix $(OBJ_PATH),$(OBJ))


all : make $(NAME)

$(NAME) : $(OBJS)
	gcc $(CFLAGS) -o $@ -L $(LIBPATH) $(OBJS) $(LDFLAGS)

make :
	make -C libft/

makeclean :
	make -C libft/ fclean

objs/%.o : srcs/%.c
	gcc $(CFLAGS) -c $^ -o $@ -I $(INCPATH) -I $(INCLIB)

fclean : clean
	rm -f $(NAME)

clean :
	rm -f $(OBJS)

re : fclean all
