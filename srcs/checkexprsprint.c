/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checkexprsprint.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmeier <nmeier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 14:28:42 by nmeier            #+#    #+#             */
/*   Updated: 2015/05/07 14:30:33 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"
#include "cluedo.h"

void		printrighttreeresult(char query, t_env env, int level, int ret)
{
	if (env.opts & VERBOSE)
	{
		print_tabs(level);
		if (env.opts & COLOR)
		{
			ft_putstr(C_UNDERLINE);
			if (ret == TRUE)
				ft_putstr(C_GREEN);
			else if (ret == FALSE)
				ft_putstr(C_RED);
		}
		if (env.opts & CLUEDO)
			ft_putendl(fact_string(query));
		else
		{
			ft_putstr("So ");
			ft_putchar(query);
			ft_putstr(" is ");
			ft_putendl(strvalue(ret));
		}
		if (env.opts & COLOR)
			ft_putstr(C_NONE);
	}
}

void		printtrueexpression(t_expr *expr, t_env env, int level)
{
	if (env.opts & VERBOSE)
	{
		print_tabs(level);
		if (env.opts & COLOR)
			ft_putstr(C_GREEN);
		ft_putstr("So expression \"");
		print_r(expr->right_side, level);
		ft_putendl("\" is true");
		if (env.opts & COLOR)
			ft_putstr(C_NONE);
	}
}

void		printreslist(char *rets, int count, int level, t_env env)
{
	int i;

	if (env.opts & VERBOSE)
	{
		print_tabs(level);
		ft_putstr("[");
		i = 0;
		while (i < count)
		{
			if (i > 0)
				ft_putstr(", ");
			ft_putstr(strvalue(rets[i]));
			i += 1;
		}
		ft_putstr("]");
		ft_putstr(" -> ");
	}
}
