/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   miscprint.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmeier <nmeier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 14:46:12 by nmeier            #+#    #+#             */
/*   Updated: 2015/05/07 14:46:18 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"

void	printeval(char query, t_expr *expr, t_env env, int level)
{
	if ((env.opts & VERBOSE) == 0)
		return ;
	print_tabs(level);
	if (env.opts & COLOR)
	{
		ft_putstr(C_UNDERLINE);
		ft_putstr(C_CYAN);
	}
	ft_putstr("<- Evaluating \"");
	print_r(expr->left_side, 0);
	ft_putstr(" => ");
	print_r(expr->right_side, 0);
	ft_putstr("\" for ");
	ft_putchar(query);
	ft_putendl(":");
	if (env.opts & COLOR)
		ft_putstr(C_NONE);
}

void	print_verbose_xor_or(int level, t_array *query, t_array *tree, int ret)
{
	print_tabs(level);
	ft_putstr("So \"");
	print_r(query, level);
	ft_putstr("\" has to be ");
	ft_putstr(strvalue(ret));
	ft_putstr(" for \"");
	print_r(tree, level);
	ft_putendl("\" to be true");
}
