/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checkexprs.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmeier <nmeier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 14:26:59 by nmeier            #+#    #+#             */
/*   Updated: 2015/05/07 14:28:10 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"

static int		exprused(int exprid, t_env env)
{
	int i;

	i = 0;
	while (i < *env.usedexprcount)
	{
		if (env.usedexprs[i] == exprid)
			return (TRUE);
		i++;
	}
	return (FALSE);
}

static int		checkret(char *rets, int count, int level, t_env env)
{
	int i;
	int j;
	int fret;

	printreslist(rets, count, level, env);
	i = 0;
	fret = NOEXPR;
	while (i < count)
	{
		if (rets[(j = i)] != UNKNOWN)
		{
			while (++j < count)
				if (j != i && rets[j] != UNKNOWN && rets[i] != rets[j])
					ft_error("Contradiction in ruleset", NULL);
			if (env.opts & VERBOSE)
				ft_putendl(strvalue(rets[i]));
			return (rets[i]);
		}
		else if (env.validation == TRUE)
			fret = UNKNOWN;
		i += 1;
	}
	if (env.opts & VERBOSE)
		ft_putendl(strvalue(fret));
	return (fret);
}

static int		checkcache(int ret, char query, t_env env, int level)
{
	int oret;

	if (ret == NOEXPR)
		oret = FALSE;
	else
		oret = ret;
	if (oret == TRUE || oret == FALSE)
	{
		if (env.cache[query - 'A'] != UNKNOWN && oret != env.cache[query - 'A'])
		{
			printrighttreeresult(query, env, level, oret);
			ft_error("Contradiction in ruleset", NULL);
		}
		env.cache[query - 'A'] = oret;
	}
	return (ret);
}

int				checkexprs(char query, t_env env, int level)
{
	t_expr			*l;
	unsigned int	i;
	char			results[env.exprcount];

	l = env.list;
	i = 0;
	while (l != NULL)
	{
		if (!exprused(l->id, env) && treehassymbol(query, l->right_side) == 1)
		{
			env.usedexprs[(*env.usedexprcount)++] = l->id;
			printeval(query, l, env, level);
			if (evaltree(l->left_side, env, level) == TRUE)
			{
				printtrueexpression(l, env, level);
				results[i] = evalrighttree(query, l->right_side, env, level);
				printrighttreeresult(query, env, level, results[i++]);
			}
			else
				results[i++] = UNKNOWN;
			*env.usedexprcount -= 1;
		}
		l = l->next;
	}
	return (checkcache(checkret(results, i, level, env), query, env, level));
}
