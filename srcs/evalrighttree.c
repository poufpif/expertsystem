/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   evalrighttree.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmeier <nmeier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/05 17:00:51 by nmeier            #+#    #+#             */
/*   Updated: 2015/05/07 14:33:34 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expert.h"

static int	eval_and(char query, t_array *tree, t_env env, int level)
{
	unsigned int	i;
	t_array			*child;

	i = 0;
	while (i < tree->size)
	{
		child = array_get(tree, i);
		if (treehassymbol(query, child) == TRUE)
			return (evalrighttree(query, child, env, level));
		i++;
	}
	return (TRUE);
}

static int	eval_or(char query, t_array *tree, t_env env, int level)
{
	unsigned int	i;
	int				ret;
	t_array			*child;
	t_array			*qchild;

	i = -1;
	while (++i < tree->size)
	{
		child = array_get(tree, i);
		if (treehassymbol(query, child) == TRUE)
		{
			qchild = child;
			continue ;
		}
		else
			ret = evaltree(child, env, level);
		if (ret != FALSE)
			return (UNKNOWN);
	}
	if (env.opts & VERBOSE)
		print_verbose_xor_or(level, qchild, tree, TRUE);
	return (evalrighttree(query, qchild, env, level));
}

static int	getvalcount(int *rets, int count, int value)
{
	int i;

	i = 0;
	while (count > 0)
	{
		count--;
		if (rets[count] == value)
			i++;
	}
	return (i);
}

static int	eval_xor(char query, t_array *tree, t_env env, int level)
{
	int				ret;
	unsigned int	i;
	int				rets[tree->size];
	t_array			*child;
	t_array			*qchild;

	i = -1;
	while (++i < tree->size)
	{
		if (treehassymbol(query, (child = array_get(tree, i))) == TRUE)
		{
			if (child->symbol != query && (qchild = child))
				ret = evalrighttree(query, child, env, level);
			else if ((rets[i] = TRUE) == TRUE)
				continue ;
		}
		else
			ret = evaltree(child, env, level);
		if ((rets[i] = ret) == UNKNOWN)
			return (UNKNOWN);
	}
	ret = (getvalcount(rets, i, TRUE) % 2 ? TRUE : FALSE);
	if (env.opts & VERBOSE)
		print_verbose_xor_or(level, qchild, tree, ret);
	return (ret);
}

int			evalrighttree(char query, t_array *tree, t_env env, int lvl)
{
	if (tree->symbol == query)
		return (TRUE);
	if (tree->symbol == '+')
		return (eval_and(query, tree, env, lvl));
	if (tree->symbol == '|')
		return (eval_or(query, tree, env, lvl));
	if (tree->symbol == '^')
		return (eval_xor(query, tree, env, lvl));
	if (tree->symbol == '!')
		return (expertnot(evalrighttree(query, array_get(tree, 0), env, lvl)));
	else
		return (resolvequery(tree->symbol, env, lvl + 1));
}
