/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dynstr.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmeier <nmeier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 15:03:59 by nmeier            #+#    #+#             */
/*   Updated: 2015/05/07 15:04:01 by nmeier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DYNSTR_H
# define DYNSTR_H

typedef struct		s_dynstr
{
	char			*chars;
	unsigned int	cap;
	unsigned int	size;
}					t_dynstr;

t_dynstr			*dynstr_alloc(char *str, unsigned int cap);
void				dynstr_destroy(t_dynstr *dynstr);
void				dynstr_append(t_dynstr *dynstr, char *src);
void				dynstr_appendchar(t_dynstr *dynstr, char c);
void				dynstr_clear(t_dynstr *dynstr);

#endif
